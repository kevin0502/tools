package com.javarequest;
import com.tool.IdNumTool;
import org.apache.jmeter.config.Arguments;
import org.apache.jmeter.protocol.java.sampler.JavaSamplerClient;
import org.apache.jmeter.protocol.java.sampler.JavaSamplerContext;
import org.apache.jmeter.samplers.SampleResult;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


public class CardTool implements JavaSamplerClient {
    //调试
    public static void main(String[]args){
        //模拟JMeter执行调用


        CardTool cardtool = new CardTool();
        Arguments arguments = new Arguments();
        JavaSamplerContext context = new JavaSamplerContext(cardtool.getDefaultParameters());
        cardtool.setupTest(context);
        cardtool.runTest(context);
        cardtool.teardownTest(context);
    }

    //setupTest  每个线程执行一次
    @Override
    public void setupTest(JavaSamplerContext javaSamplerContext) {
        System.out.println("-----------------java请求：setupTest-----------------");
    }

    //执行业务，一个sample请求
    //这个函数是有返回值的
    @Override
    public SampleResult runTest(JavaSamplerContext javaSamplerContext) {
        System.out.println("-----------------java请求：runTest-----------------");

        SampleResult sampleResult = new SampleResult();
        sampleResult.setSampleLabel("生成身份证");
        //请求开始
        sampleResult.sampleStart();

        //调用idNumTool.getidcard方法，生成身份证号
        String response;
        IdNumTool idNumTool  = new IdNumTool();
        String idcard = idNumTool.getidcard(javaSamplerContext.getParameter("card_code"),javaSamplerContext.getParameter("card_birthday"),javaSamplerContext.getParameter("card_sex"));

        //设置请求内容
        String samplerdata ="card_code"+":"+ javaSamplerContext.getParameter("card_code")+","+"card_birthday"+":"+ javaSamplerContext.getParameter("card_birthday")+","+"card_sex"+":"+javaSamplerContext.getParameter("card_sex");

        sampleResult.setSamplerData(samplerdata);

        if(idcard.length() == 18){
            sampleResult.setSuccessful(true); //使采样器请求成功（表变绿）
            response = "{\"code\":0,\"msg\":\"请求成功\",\"idcard\": "+ idcard+" }";
        }else {
            sampleResult.setSuccessful(false); //使采样器请求失败（变红）
            response = "{\"code\":500,\"msg\":\"身份证生产异常\"}";
        }
        sampleResult.setResponseData(response,"UTF-8");

        return sampleResult;
    }

    //teardownTest 每个线程执行一次
    @Override
    public void teardownTest(JavaSamplerContext javaSamplerContext) {
        System.out.println("-----------------java请求：teardownTest-----------------");
    }

    @Override
    public Arguments getDefaultParameters() {
        Arguments arguments = new Arguments();
        //参数code跟默认值
        arguments.addArgument("card_code","430922");
        arguments.addArgument("card_birthday","1992-10-01");
        arguments.addArgument("card_sex","1");

        return arguments;
    }
}
