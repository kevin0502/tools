package com.functions;

import com.jayway.jsonpath.JsonPath;
import org.apache.jmeter.engine.util.CompoundVariable;
import org.apache.jmeter.functions.AbstractFunction;
import org.apache.jmeter.functions.InvalidVariableException;
import org.apache.jmeter.samplers.SampleResult;
import org.apache.jmeter.samplers.Sampler;

import java.util.*;

import org.json.JSONObject;
import org.yaml.snakeyaml.Yaml;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class ReadYaml extends AbstractFunction {
    public String json_path = "";
    public String yaml_path = "";

    public static void main(String[] args) throws FileNotFoundException {
        Yaml yaml = new Yaml();
        File file= new File("data/config.yml");
//        Map<String, Map<String, String>> conf = yaml.load(new FileInputStream(file));
//        System.out.println(conf);
//        System.out.println(conf.get("sit").get("consult_databasa_url"));

        FileInputStream fis = new FileInputStream(file);
        Map<String, Object> loaded = (Map<String, Object>) yaml.load(fis);
        String header_str = new JSONObject(loaded).toString();
        String data = JsonPath.read(header_str,"$.sit.test").toString();
        System.out.println("data = " + data);
        
//        String json_path = "$.sit1.test";
//        if (json_path.contains("sit1") || json_path.contains("sit2")){
//            json_path = json_path.replaceAll("sit(.{1})","sit");
//        }
//        System.out.println(json_path);
    }

    @Override
    public String execute(SampleResult sampleResult, Sampler sampler) throws InvalidVariableException {
        String response;
        Yaml yaml = new Yaml();
        File file= new File(yaml_path);
        FileInputStream yaml_fis;
//        System.out.println("取到的json表达式："+json_path);
        try {
            yaml_fis = new FileInputStream(file);
            Map<String, Object> loaded = (Map<String, Object>) yaml.load(yaml_fis);
            String header_str = new JSONObject(loaded).toString();
            // 提取后转为String 不然提取数字是会报错
            response = JsonPath.read(header_str,json_path).toString();
            System.out.println("yaml文件转为Json后的格式："+header_str);

        } catch (FileNotFoundException e) {
            throw new RuntimeException("yaml文件取值有误："+e);
        }
        return response;
    }

    @Override
    public void setParameters(Collection<CompoundVariable> collection) throws InvalidVariableException {
        Object[] parmsData = collection.toArray();
        try {
            json_path = ((CompoundVariable)parmsData[0]).execute();
            System.out.println("json_path处理前："+json_path);
            if (json_path.contains("sit1") || json_path.contains("sit2")){
                json_path = json_path.replaceAll("sit(.{1})","sit");
            }
            System.out.println("json_path处理后："+json_path);
            yaml_path = ((CompoundVariable)parmsData[1]).execute();
        } catch(Exception e){
            System.out.println(e);
        }
        if ("".equals(yaml_path)){
            yaml_path = "..\\user_file\\config.yml";
        }
    }

    @Override
    public String getReferenceKey() {
        String  key = "__ReadYaml";
        return key;
    }

    @Override
    public List<String> getArgumentDesc() {
        List<String> parms = new LinkedList<String>();
        parms.add("json_path表达式（内部已把yaml转为json格式）");
        parms.add("yaml文件路径（默认取user_file\\config.yml）");

        return parms;
    }
}
