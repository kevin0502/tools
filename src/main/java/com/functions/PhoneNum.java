package com.functions;

import org.apache.jmeter.engine.util.CompoundVariable;
import org.apache.jmeter.functions.AbstractFunction;
import org.apache.jmeter.functions.InvalidVariableException;
import org.apache.jmeter.samplers.SampleResult;
import org.apache.jmeter.samplers.Sampler;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

public class PhoneNum extends AbstractFunction {
    public String StaNum = "";

    @Override
    public String execute(SampleResult sampleResult, Sampler sampler) throws InvalidVariableException {
        //获取一个ms的时间戳
        String timeStampMs = String.valueOf(System.currentTimeMillis());
        String phone = StaNum + timeStampMs.substring(StaNum.length()+2,13);

        return phone;
    }

    @Override
    public void setParameters(Collection<CompoundVariable> collection) throws InvalidVariableException {
        try {
            Object[] parmsData = collection.toArray();
            StaNum = ((CompoundVariable)parmsData[0]).execute();
        } catch(Exception e){
            StaNum = "144";
        }
    }

    @Override
    public String getReferenceKey() {
        String key = "__PhoneNum";
        return key;
    }

    @Override
    public List<String> getArgumentDesc() {
        List<String> parms = new LinkedList<String>();
        parms.add("手机号以什么开头(默认144)");
        return parms;
    }
}
