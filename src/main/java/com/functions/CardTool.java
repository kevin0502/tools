package com.functions;

import com.tool.IdNumTool;
import org.apache.jmeter.engine.util.CompoundVariable;
import org.apache.jmeter.functions.AbstractFunction;
import org.apache.jmeter.functions.InvalidVariableException;
import org.apache.jmeter.samplers.SampleResult;
import org.apache.jmeter.samplers.Sampler;

import java.text.SimpleDateFormat;
import java.util.*;

public class CardTool extends AbstractFunction {

    public String card_code = "";
    public String card_birthday = "";
    public String card_sex = "";

    //定义函数参数列表
    @Override
    public List<String> getArgumentDesc() {
        List<String> parms = new LinkedList<String>();
        parms.add("请输入地区码：440304（默认）");
        parms.add("请输入生日（格式：xxxx-xx-xx，默认24年前的今天）");
        parms.add("请输入性别（男：1（默认），女：2）");
        return parms;
    }

    //用来接收、处理用户调用函数时所传入的参数值
    @Override
    public void setParameters(Collection<CompoundVariable> collection) throws InvalidVariableException {
        //collection为getArgumentDesc函数接收到的用户输入的值
        //检查用户输入的参数值是否等于3个
        checkParameterCount(collection, 3);
        //把Collection<CompoundVariable>转换为数组，固定写法
        Object[] parmsData = collection.toArray();

        //把data对象取值做CompoundVariable类型的强制转换，再用execute把值转为String类型
        card_code = ((CompoundVariable) parmsData[0]).execute();
        card_birthday = ((CompoundVariable) parmsData[1]).execute();
        card_sex = ((CompoundVariable) parmsData[2]).execute();
    }

    //函数的执行主体，执行具体的业务逻辑、功能
    @Override
    public String execute(SampleResult sampleResult, Sampler sampler) throws InvalidVariableException {
        //初始化idNumTool类
        Calendar calendar = Calendar.getInstance();// 得到一个Calendar的实例
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd"); //初始化时间格式
        calendar.add(Calendar.YEAR, -24);
        Date resultDate = calendar.getTime();
        if ("".equals(card_birthday)){
            card_birthday = sdf.format(resultDate);
        }
        if ("".equals(card_code)){
            card_code = "440304";
        }
        if ("".equals(card_birthday)){
            card_sex = "1";
        }
        IdNumTool idNumTool  = new IdNumTool();
        String idcard = idNumTool.getidcard(card_code,card_birthday,card_sex);

        return idcard;
    }
    //要调用的函数名称
    @Override
    public String getReferenceKey() {
        String key = "__IdCard";
        return key ;
    }
}
