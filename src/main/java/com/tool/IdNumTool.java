package com.tool;

import java.util.*;
import java.util.stream.Collectors;

public class IdNumTool {
    //生成地区编码

    public String region(String region){
        if(region.equals("")){
            return "110000";
        }else{
            return region;
        }
    }
    //生成出生年月日
    public String birthday(String birthday){
        String birth_day = birthday.replace("-","");
        return birth_day;
    }
    //生成随机的3位
    public int getthreenum(){
        int threenumber = (int)(Math.random()*1000);
        if (threenumber < 100 || threenumber>999){
            threenumber = getthreenum();
        }
        return threenumber;
    }
    //生成指定性别的三位数
    public String randomSex(String sex){
        //1男  2女
        int threenumber = getthreenum();
        if(sex.equals("1")){
            if (threenumber % 2==0){
                threenumber = threenumber +1;
                return threenumber+"";
            }
            else{
                return threenumber+"";
            }
        }
        else if(sex.equals("2")){
            if (threenumber % 2==0){
                return threenumber+"";
            }else{
                threenumber = threenumber +1;
                return threenumber+"";
            }
        }
        return threenumber+"";
    }

    public String getidcard(String region,String birthday,String sex){
        HashMap cardmap = new HashMap();
        cardmap.put("0","1");
        cardmap.put("1","0");
        cardmap.put("2","X");
        cardmap.put("3","9");
        cardmap.put("4","8");
        cardmap.put("5","7");
        cardmap.put("6","6");
        cardmap.put("7","5");
        cardmap.put("8","4");
        cardmap.put("9","3");
        cardmap.put("10","2");

        IdNumTool idnum1 = new IdNumTool();
        String Sidcard1 = idnum1.region(region)+idnum1.birthday(birthday)+idnum1.randomSex(sex);
        String[] idcard2 = Sidcard1.split("");
        int[] cardnum = new int[0];
        List idcardlist = Arrays.stream(cardnum).boxed().collect(Collectors.toList());
        for(String each : idcard2) {
            idcardlist.add(new Integer(each));
        }
        int[] data = new int[] {7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2};

        int sum = 0;
        for(int i=0;i<17;i++){
            sum = sum + Integer.parseInt((idcardlist.get(i).toString())) * data[i];
        }
        String lastnum = "";
        lastnum=cardmap.get(sum%11+"")+"";
        return Sidcard1+lastnum;
    }

    public static void main(String[] args) {
        IdNumTool idNumTool  = new IdNumTool();
        String appnt_idcard = idNumTool.getidcard("430921","1990-01-01","1");
        System.out.println(appnt_idcard);
    }

}
