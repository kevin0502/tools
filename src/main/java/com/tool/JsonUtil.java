package com.tool;
import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.Configuration;
import com.jayway.jsonpath.PathNotFoundException;

import java.util.ArrayList;
import java.util.List;

public class JsonUtil {
//    public  String ParaseJson(String str,String paras){
//        String res = JsonPath.read(str,paras).toString();
//        return  res;
//
//    }
    public  String ParaseJson(String str,String paras){
        String res = "";
        try {
            res = JsonPath.read(str, paras).toString();
        } catch (PathNotFoundException e) {
            res = "";
        }
        return  res;

    }
    public Object Read(String str, String paras){
        Object document = Configuration.defaultConfiguration().jsonProvider().parse(str);
        return JsonPath.read(document,paras);
    }

    public static void main(String[] args) {
        String a = "{\"code\":60000,\"msg\":\"OK\",\"data\":{\"token\":\"SRlzvZOkXLdH3sPsY5khwX90m3b8XZMHRLk2FDThD1uDe1LxeygglbCiUoll5a7nqIhSjXYfNO3J0toRDefimOMfxkPvBLkJUro78FJeFifDmJCeNB8FYtv2wDYAbCIj\",\"status\":1,\"accountType\":1,\"existSub\":false,\"authListUserInfoResList\":[{\"userId\":84569,\"userName\":\"uat-consult-admin\",\"accountType\":1,\"status\":1,\"isTrue\":true,\"roleList\":[{\"roleName\":\"管理员\",\"label\":\"administrator\"},{\"roleName\":\"回访管理员\",\"label\":\"revisit_admin\"},{\"roleName\":\"管理员\",\"label\":\"administrator\"},{\"roleName\":\"回访管理员\",\"label\":\"revisit_admin\"}]}],\"appid\":null,\"panshiUrl\":null}}";
        String getJsonPath ="$..roleName";
        JsonUtil jsonutil = new JsonUtil();
        String b = jsonutil.ParaseJson(a,getJsonPath);
        System.out.println("b0 = " + b);

        Object read = jsonutil.Read(a,getJsonPath);
        System.out.println("b1 = " + read.toString());

        if (read instanceof List) {
            List<?> list = (List<?>) read;
            // 现在，您可以使用 list 对象进行操作
            for (Object aa : list) {
                System.out.println(aa);
            }
            // ...
        } else {
            System.out.println("啥也不是");
            // obj 不是列表类型
            // 处理相应的逻辑
        }






    }
}